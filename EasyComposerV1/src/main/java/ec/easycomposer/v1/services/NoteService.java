package ec.easycomposer.v1.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.easycomposer.v1.models.Note;
import ec.easycomposer.v1.repositories.NoteRepository;

@Service
public class NoteService {

	@Autowired
	private NoteRepository repository;

	/*
	 * hz=440*(Math.pow(2, (entity.getDemiTons()/12)) recupération de la frequence
	 * par rapport au demi tons : hz de Note++ = hz*1,0594630943
	 */

	// calcule l'écart en demi tons par rapport au E1 82.4
	public int log2(float x) {
		return (int) Math.round((12 * (Math.log(x / 82.4) / Math.log(2))));
	}

	public List<Note> choixGamme(String s, String nom) {
		List<Note> gamme = new ArrayList<>();
		Note note = findByName(nom);
		System.out.println(s + " " + nom + " " + note);
		if (s.equalsIgnoreCase("major")) {
			System.out.println("major");
			gamme = gammeMaj(note);
		} else if (s.equalsIgnoreCase("minor")) {
			gamme = gammeMin(note);
		} else if (s.equalsIgnoreCase("pentaMajor")) {
			gamme = gammePentaMaj(note);
		} else if (s.equalsIgnoreCase("pentaMinor")) {
			gamme = gammePentaMin(note);
		}
		return gamme;
	}

	public List<Note> gammeMaj(Note note) {
		List<Note> list = this.repository.findAll();
		List<Note> gamme = new ArrayList<>();

		float d = note.getDemiTons();
		gamme.add(note);
		for (Note note2 : list) {
			float t = note2.getDemiTons();
			if (t == d + 2 || t == d + 4 || t == d + 5 || t == d + 7 || t == d + 9 || t == d + 11 || t == d + 12) {
				System.out.println("trouvé");
				gamme.add(note2);
			} else {
				System.out.println("nope");
			}
		}
		return gamme;
	}

	public List<Note> gammeMin(Note note) {
		List<Note> list = this.repository.findAll();
		List<Note> gamme = new ArrayList<>();

		float d = note.getDemiTons();
		gamme.add(note);
		for (Note note2 : list) {
			float t = note2.getDemiTons();
			if (t == d + 2 || t == d + 3 || t == d + 5 || t == d + 7 || t == d + 8 || t == d + 10 || t == d + 12) {
				System.out.println("trouvé");
				gamme.add(note2);
			} else {
				System.out.println("nope");
			}
		}
		return gamme;
	}

	public List<Note> gammePentaMaj(Note note) {
		List<Note> list = this.repository.findAll();
		List<Note> gamme = new ArrayList<>();

		float d = note.getDemiTons();
		gamme.add(note);
		for (Note note2 : list) {
			float t = note2.getDemiTons();
			if (t == d + 2 || t == d + 4 || t == d + 7 || t == d + 9 || t == d + 12) {
				gamme.add(note2);

			}

		}
		return gamme;
	}

	public List<Note> gammePentaMin(Note note) {
		List<Note> list = this.repository.findAll();
		List<Note> gamme = new ArrayList<>();

		float d = note.getDemiTons();
		gamme.add(note);
		for (Note note2 : list) {
			float t = note2.getDemiTons();
			if (t == d + 3 || t == d + 5 || t == d + 7 || t == d + 10 || t == d + 12) {
				gamme.add(note2);

			}

		}
		return gamme;
	}

	public List<Note> gammeDiatonique(Note note) {
		List<Note> list = this.repository.findAll();
		List<Note> gamme = new ArrayList<>();

		float d = note.getDemiTons();
		gamme.add(note);
		for (Note note2 : list) {
			float t = note2.getDemiTons();
			if (t == d + 2 || t == d + 4 || t == d + 5 || t == d + 7 || t == d + 9 || t == d + 11 || t == d + 12) {
				gamme.add(note2);
			}
		}
		return gamme;
	}

	public List<Note> listeNote() {
		List<Note> listNote = new ArrayList<>();
		float hz = 32.70f;
		Note note = new Note("C0", "C", 0, hz, (hz * 0.97f), (hz * 1.035f), log2(hz), null);
		listNote.add(note);
		for (int i = 1; i < 120; i++) {
			Note n = new Note();
			Note r = listNote.get(i - 1);
			if (r.getType() == "E") {
				n.setType("F");
			} else if (r.getType() == "F") {
				n.setType("Gb");
			} else if (r.getType() == "Gb") {
				n.setType("G");
			} else if (r.getType() == "G") {
				n.setType("Ab");
			} else if (r.getType() == "Ab") {
				n.setType("A");
			} else if (r.getType() == "A") {
				n.setType("Bb");
			} else if (r.getType() == "Bb") {
				n.setType("B");
			} else if (r.getType() == "B") {
				n.setType("C");
			} else if (r.getType() == "C") {
				n.setType("Db");
			} else if (r.getType() == "Db") {
				n.setType("D");
			} else if (r.getType() == "D") {
				n.setType("Eb");
			} else if (r.getType() == "Eb") {
				n.setType("E");
			}
			n.setHz(r.getHz() * 1.0594630943f); // set la frequence par rapport à la note precedente
			n.setHzMin(n.getHz() * 0.97f);
			n.setHzMax(n.getHz() * 1.035f);
			n.setDemiTons(log2(n.getHz()));
			if (n.getHz() > 30f && n.getHz() < 63f) {
				n.setOctave(0);
			} else if (n.getHz() > 64f && n.getHz() < 124f) {
				n.setOctave(1);
			} else if (n.getHz() > 125f && n.getHz() < 248f) {
				n.setOctave(2);
			} else if (n.getHz() > 249f && n.getHz() < 495f) {
				n.setOctave(3);
			} else if (n.getHz() > 496f && n.getHz() < 989f) {
				n.setOctave(4);
			} else if (n.getHz() > 990f && n.getHz() < 1977f) {
				n.setOctave(5);
			} else if (n.getHz() > 1978f && n.getHz() < 3970f) {
				n.setOctave(6);
			} else if (n.getHz() > 4100f && n.getHz() < 8000f) {
				n.setOctave(7);
			} else if (n.getHz() > 8100f && n.getHz() < 16000f) {
				n.setOctave(8);
			} else if (n.getHz() > 16500f && n.getHz() < 32000f) {
				n.setOctave(9);
			}
			n.setNom(n.getType() + n.getOctave());
			listNote.add(n);
		}
		return listNote;
	}

	public Note save(Note entity) {
		// entity.setNom(entity.getType() + entity.getOctave());
		// entity.setDemiTons(log2(entity.getHz()));

		return this.repository.save(entity);

	}

	public void saveNotes() {
		List<Note> list = listeNote();
		for (Note note : list) {
			this.repository.save(note);
		}
	}

	public List<Note> findAll() {
		return this.repository.findAll();
	}

	public Optional<Note> findById(String id) {
		return this.repository.findById(id);
	}

	// les findByHz fonctionnent pas
	public Note findByHz(float hz) {
		List<Note> list = this.repository.findAll();
		for (Note note : list) {
			if (hz > note.getHzMin() && hz < note.getHzMax()) {
				hz = note.getHz();
			}
		}
		return this.repository.findByHz(hz);
	}

	public Optional<Note> findByMinHz(float hz) {
		return this.repository.findByHzMin(hz);
	}

	public Optional<Note> findByMaxHz(float hz) {
		return this.repository.findByHzMax(hz);
	}

	public Note findByName(String nom) {
		return this.repository.findByNom(nom);
	}

}
