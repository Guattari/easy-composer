package ec.easycomposer.v1.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.easycomposer.v1.models.Utilisateur;
import ec.easycomposer.v1.repositories.UtilisateurRepository;

@Service
public class UtilisateurService {

	@Autowired
	private UtilisateurRepository repository;

	public List<Utilisateur> findAllByName(String name) {
		return this.repository.findAllByName(name);
	}

	public List<Utilisateur> findAllByUsername(String username) {
		return this.repository.findAllByUsername(username);
	}

	public List<Utilisateur> findAllByNameOrFname(String name, String fname) {
		return this.repository.findAllByNameOrFname(name, fname);
	}

	public List<Utilisateur> findByEmail(String email) {
		return this.repository.findByEmail(email);
	}

	public List<Utilisateur> findAllByTrancheAge(String trancheAge) {
		return this.repository.findAllByTrancheAge(trancheAge);
	}


}
