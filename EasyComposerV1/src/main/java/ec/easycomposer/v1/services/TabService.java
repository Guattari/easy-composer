package ec.easycomposer.v1.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.easycomposer.v1.models.Note;
import ec.easycomposer.v1.models.Tab;
import ec.easycomposer.v1.repositories.TabRepository;

@Service
public class TabService {
		
	@Autowired
	private TabRepository repository;

	/*public List<Tab> findByTitre(String titre) {
		return this.repository.findByTitre(titre);
	} */

	public void positionsNotes(Note n, List<Note> note) {
		List<Note> list = this.repository.findAll();
		for (Note n1 : list) {
			if (n1.getNom() == "C") {
				int[] pos = { 8, 13, 29, 46, 51, 68 };
				n1.setPositions(pos);
			} else if (n1.getNom() == "D") {
				int[] pos = { 10, 15, 31, 48, 53, 70 };
				n1.setPositions(pos);
			} else if (n1.getNom() == "E") {
				int[] pos = { 12, 17, 33, 38, 55, 72 };
				n1.setPositions(pos);
			} else if (n1.getNom() == "F") {
				int[] pos = { 1, 18, 34, 39, 56, 61 };
				n1.setPositions(pos);
			} else if (n1.getNom() == "G") {
				int[] pos = { 3, 20, 36, 41, 58, 63 };
				n1.setPositions(pos);
			} else if (n1.getNom() == "A") {
				int[] pos = { 5, 22, 26, 43, 60, 65 };
				n1.setPositions(pos);
			} else if (n1.getNom() == "B") {
				int[] pos = { 7, 24, 28, 45, 50, 67 };
				n1.setPositions(pos);
			}
		}

	}

	public void choixPosition(Note n, List<Note> note) {
		List<Note> list = this.repository.findAll();

		for (Note n1 : list) {
			for (int e : n.getPositions()) {
				if (n.getPositions()[0] == 13 || n.getPositions()[0] == 15) { // position 2 C/D
					if (n.getNom() == "D") {
						int pos[] = { 15 };
					} else if (n.getNom() == "E") {
						int pos[] = { 17 };
					} else if (n.getNom() == "F") {
						int pos[] = { 18 };
					} else if (n.getNom() == "G") {
						int pos[] = { 20 };
					} else if (n.getNom() == "A") {
						int pos[] = { 26 };
					} else if (n.getNom() == "B") {
						int pos[] = { 28 };
					} else if (n.getNom() == "C") {
						int pos[] = { 13 };
					}
				} else if (n.getPositions()[0] == 8 || n.getPositions()[0] == 10) { // position 1 C/D
					if (n.getNom() == "D") {
						int pos[] = { 10 };
					} else if (n.getNom() == "E") {
						int pos[] = { 17 };
					} else if (n.getNom() == "F") {
						int pos[] = { 18 };
					} else if (n.getNom() == "G") {
						int pos[] = { 20 };
					} else if (n.getNom() == "A") {
						int pos[] = { 22 };
					} else if (n.getNom() == "B") {
						int pos[] = { 7 };
					} else if (n.getNom() == "C") {
						int pos[] = { 8 };
					}
				} else if (n.getPositions()[0] == 29 || n.getPositions()[0] == 31) { // position 3 C/D
					if (n.getNom() == "D") {
						int pos[] = { 31 };
					} else if (n.getNom() == "E") {
						int pos[] = { 33 };
					} else if (n.getNom() == "F") {
						int pos[] = { 34 };
					} else if (n.getNom() == "G") {
						int pos[] = { 41 };
					} else if (n.getNom() == "A") {
						int pos[] = { 26 };
					} else if (n.getNom() == "B") {
						int pos[] = { 28 };
					} else if (n.getNom() == "C") {
						int pos[] = { 29 };
					}
				} else if (n.getPositions()[0] == 46 || n.getPositions()[0] == 48) { // position 4 C/D
					if (n1.getNom() == "D") {
						int pos[] = { 48 };
					} else if (n1.getNom() == "E") {
						int pos[] = { 55 };
					} else if (n1.getNom() == "F") {
						int pos[] = { 56 };
					} else if (n1.getNom() == "G") {
						int pos[] = { 58 };
					} else if (n1.getNom() == "A") {
						int pos[] = { 43 };
					} else if (n1.getNom() == "B") {
						int pos[] = { 45 };
					} else if (n1.getNom() == "C") {
						int pos[] = { 46 };
					} else if (n.getPositions()[0] == 51 || n.getPositions()[0] == 53) { // position 5 C/D
						if (n.getNom() == "D") {
							int pos[] = { 53 };
						} else if (n.getNom() == "E") {
							int pos[] = { 55 };
						} else if (n.getNom() == "F") {
							int pos[] = { 56 };
						} else if (n.getNom() == "G") {
							int pos[] = { 58 };
						} else if (n.getNom() == "A") {
							int pos[] = { 65 };
						} else if (n.getNom() == "B") {
							int pos[] = { 45 };
						} else if (n.getNom() == "C") {
							int pos[] = { 51 };
						}
					} else if (n.getPositions()[0] == 68 || n.getPositions()[0] == 70) { // position 6 C/D
						if (n.getNom() == "D") {
							int pos[] = { 70 };
						} else if (n.getNom() == "E") {
							int pos[] = { 72 };
						} else if (n.getNom() == "F") {
							int pos[] = { 39 };
						} else if (n.getNom() == "G") {
							int pos[] = { 63 };
						} else if (n.getNom() == "A") {
							int pos[] = { 65 };
						} else if (n.getNom() == "B") {
							int pos[] = { 67 };
						} else if (n.getNom() == "C") {
							int pos[] = { 68 };
						}
					} else if (n.getPositions()[0] == 12) { // position 1 E
						if (n.getNom() == "D") {
							int pos[] = { 10 };
						} else if (n.getNom() == "C") {
							int pos[] = { 8 };
						} else if (n.getNom() == "E") {
							int pos[] = { 12 };
						} else if (n.getNom() == "F") {
							int pos[] = { 18 };
						} else if (n.getNom() == "G") {
							int pos[] = { 20 };
						} else if (n.getNom() == "A") {
							int pos[] = { 22 };
						} else if (n.getNom() == "B") {
							int pos[] = { 24 };
						} else if (n.getPositions()[0] == 17 || n.getPositions()[0] == 18) { // position 2 E/F
							if (n1.getNom() == "E") {
								int pos[] = { 17 };
							} else if (n1.getNom() == "F") {
								int pos[] = { 18 };
							} else if (n1.getNom() == "C") {
								int pos[] = { 29 };
							} else if (n1.getNom() == "D") {
								int pos[] = { 15 };
							} else if (n1.getNom() == "A") {
								int pos[] = { 5 };
							} else if (n1.getNom() == "B") {
								int pos[] = { 28 };
							} else if (n1.getNom() == "G") {
								int pos[] = { 20 };
							}
						} else if (n.getPositions()[0] == 33 || n.getPositions()[0] == 34) { // position 3 E/F
							if (n1.getNom() == "E") {
								int pos[] = { 33 };
							} else if (n1.getNom() == "F") {
								int pos[] = { 34 };
							} else if (n1.getNom() == "C") {
								int pos[] = { 46 };
							} else if (n1.getNom() == "D") {
								int pos[] = { 31 };
							} else if (n1.getNom() == "A") {
								int pos[] = { 43 };
							} else if (n1.getNom() == "B") {
								int pos[] = { 45 };
							} else if (n1.getNom() == "G") {
								int pos[] = { 36 };
							}
						} else if (n.getPositions()[0] == 38 || n.getPositions()[0] == 39) { // position 4 E/F
							if (n1.getNom() == "E") {
								int pos[] = { 38 };
							} else if (n1.getNom() == "F") {
								int pos[] = { 39 };
							} else if (n1.getNom() == "C") {
								int pos[] = { 29 };
							} else if (n1.getNom() == "D") {
								int pos[] = { 53 };
							} else if (n1.getNom() == "A") {
								int pos[] = { 43 };
							} else if (n1.getNom() == "B") {
								int pos[] = { 28 };
							} else if (n1.getNom() == "G") {
								int pos[] = { 41 };
							}
						} else if (n.getPositions()[0] == 55 || n.getPositions()[0] == 56) { // position 5 E/F
							if (n.getNom() == "E") {
								int pos[] = { 55 };
							} else if (n.getNom() == "F") {
								int pos[] = { 56 };
							} else if (n.getNom() == "C") {
								int pos[] = { 68 };
							} else if (n.getNom() == "D") {
								int pos[] = { 53 };
							} else if (n.getNom() == "A") {
								int pos[] = { 43 };
							} else if (n.getNom() == "B") {
								int pos[] = { 45 };
							} else if (n.getNom() == "G") {
								int pos[] = { 58 };
							}
						} else if (n.getPositions()[0] == 72) { // position 6 E
							if (n.getNom() == "E") {
								int pos[] = { 72 };
							} else if (n.getNom() == "F") {
								int pos[] = { 56 };
							} else if (n.getNom() == "C") {
								int pos[] = { 68 };
							} else if (n.getNom() == "D") {
								int pos[] = { 70 };
							} else if (n.getNom() == "A") {
								int pos[] = { 60 };
							} else if (n.getNom() == "B") {
								int pos[] = { 45 };
							} else if (n.getNom() == "G") {
								int pos[] = { 58 };
							}
						} else if (n.getPositions()[0] == 61) { // position 6 F
							if (n1.getNom() == "E") {
								int pos[] = { 38 };
							} else if (n1.getNom() == "F") {
								int pos[] = { 61 };
							} else if (n1.getNom() == "C") {
								int pos[] = { 51 };
							} else if (n1.getNom() == "D") {
								int pos[] = { 53 };
							} else if (n1.getNom() == "A") {
								int pos[] = { 65 };
							} else if (n1.getNom() == "B") {
								int pos[] = { 50 };
							} else if (n1.getNom() == "G") {
								int pos[] = { 63 };
							}
						} else if (n.getPositions()[0] == 3 || n.getPositions()[0] == 5) { // position 1 G/A
							if (n1.getNom() == "G") {
								int pos[] = { 3 };
							} else if (n.getNom() == "C") {
								int pos[] = { 18 };
							} else if (n.getNom() == "D") {
								int pos[] = { 15 };
							} else if (n.getNom() == "E") {
								int pos[] = { 17 };
							} else if (n.getNom() == "F") {
								int pos[] = { 18 };
							} else if (n.getNom() == "A") {
								int pos[] = { 5 };
							} else if (n.getNom() == "B") {
								int pos[] = { 28 };
							}
						} else if (n.getPositions()[0] == 20 || n.getPositions()[0] == 22) { // position 2 G/A
							if (n1.getNom() == "G") {
								int pos[] = { 20 };
							} else if (n1.getNom() == "A") {
								int pos[] = { 22 };
							} else if (n1.getNom() == "C") {
								int pos[] = { 8 };
							} else if (n1.getNom() == "D") {
								int pos[] = { 31 };
							} else if (n.getNom() == "E") {
								int pos[] = { 33 };
							} else if (n.getNom() == "F") {
								int pos[] = { 34 };
							} else if (n.getNom() == "B") {
								int pos[] = { 24 };
							}
						} else if (n.getPositions()[0] == 36) { // position 3 G
							if (n.getNom() == "G") {
								int pos[] = { 36 };
							} else if (n.getNom() == "C") {
								int pos[] = { 46 };
							} else if (n.getNom() == "D") {
								int pos[] = { 31 };
							} else if (n.getNom() == "E") {
								int pos[] = { 33 };
							} else if (n.getNom() == "F") {
								int pos[] = { 34 };
							} else if (n.getNom() == "A") {
								int pos[] = { 22 };
							} else if (n.getNom() == "B") {
								int pos[] = { 45 };
							}
						} else if (n.getPositions()[0] == 26) { // position 3 A
							if (n.getNom() == "A") {
								int pos[] = { 26 };
							} else if (n.getNom() == "C") {
								int pos[] = { 29 };
							} else if (n.getNom() == "D") {
								int pos[] = { 31 };
							} else if (n.getNom() == "E") {
								int pos[] = { 38 };
							} else if (n.getNom() == "F") {
								int pos[] = { 39 };
							} else if (n.getNom() == "B") {
								int pos[] = { 28 };
							} else if (n.getNom() == "G") {
								int pos[] = { 41 };
							}
						} else if (n.getPositions()[0] == 41 || n.getPositions()[0] == 43) { // position 4 A/G
							if (n.getNom() == "G") {
								int pos[] = { 41 };
							} else if (n.getNom() == "A") {
								int pos[] = { 43 };
							} else if (n.getNom() == "B") {
								int pos[] = { 45 };
							} else if (n.getNom() == "C") {
								int pos[] = { 46 };
							} else if (n.getNom() == "D") {
								int pos[] = { 31 };
							} else if (n.getNom() == "E") {
								int pos[] = { 33 };
							} else if (n.getNom() == "F") {
								int pos[] = { 39 };
							}
						} else if (n.getPositions()[0] == 58 || n.getPositions()[0] == 60) { // position 5 A/G
							if (n.getNom() == "G") {
								int pos[] = { 58 };
							} else if (n.getNom() == "A") {
								int pos[] = { 60 };
							} else if (n.getNom() == "B") {
								int pos[] = { 45 };
							} else if (n.getNom() == "C") {
								int pos[] = { 68 };
							} else if (n.getNom() == "D") {
								int pos[] = { 70 };
							} else if (n.getNom() == "E") {
								int pos[] = { 55 };
							} else if (n.getNom() == "F") {
								int pos[] = { 56 };
							}
						} else if (n.getPositions()[0] == 63 || n.getPositions()[0] == 65) { // position 6 A/G
							if (n.getNom() == "G") {
								int pos[] = { 63 };
							} else if (n.getNom() == "A") {
								int pos[] = { 65 };
							} else if (n.getNom() == "B") {
								int pos[] = { 67 };
							} else if (n.getNom() == "C") {
								int pos[] = { 51 };
							} else if (n.getNom() == "D") {
								int pos[] = { 53 };
							} else if (n.getNom() == "E") {
								int pos[] = { 38 };
							} else if (n.getNom() == "F") {
								int pos[] = { 61 };
							}
						} else if (n.getPositions()[0] == 7) { // position 1 B
							if (n.getNom() == "B") {
								int pos[] = { 7 };
							} else if (n.getNom() == "A") {
								int pos[] = { 5 };
							} else if (n.getNom() == "C") {
								int pos[] = { 8 };
							} else if (n.getNom() == "D") {
								int pos[] = { 10 };
							} else if (n.getNom() == "E") {
								int pos[] = { 17 };
							} else if (n.getNom() == "F") {
								int pos[] = { 18 };
							} else if (n.getNom() == "G") {
								int pos[] = { 20 };
							}
						} else if (n.getPositions()[0] == 24) { // position 2 B
							if (n.getNom() == "B") {
								int pos[] = { 24 };
							} else if (n.getNom() == "A") {
								int pos[] = { 22 };
							} else if (n.getNom() == "C") {
								int pos[] = { 8 };
							} else if (n.getNom() == "D") {
								int pos[] = { 10 };
							} else if (n.getNom() == "E") {
								int pos[] = { 33 };
							} else if (n.getNom() == "F") {
								int pos[] = { 34 };
							} else if (n.getNom() == "G") {
								int pos[] = { 20 };
							}
						} else if (n.getPositions()[0] == 28) { // position 3 B
							if (n.getNom() == "B") {
								int pos[] = { 28 };
							} else if (n.getNom() == "A") {
								int pos[] = { 26 };
							} else if (n.getNom() == "C") {
								int pos[] = { 29 };
							} else if (n.getNom() == "D") {
								int pos[] = { 31 };
							} else if (n.getNom() == "E") {
								int pos[] = { 17 };
							} else if (n.getNom() == "F") {
								int pos[] = { 18 };
							} else if (n.getNom() == "G") {
								int pos[] = { 41 };
							}
						} else if (n.getPositions()[0] == 45) { // position 4 B
							if (n.getNom() == "B") {
								int pos[] = { 45 };
							} else if (n.getNom() == "A") {
								int pos[] = { 43 };
							} else if (n.getNom() == "C") {
								int pos[] = { 46 };
							} else if (n.getNom() == "D") {
								int pos[] = { 48 };
							} else if (n.getNom() == "E") {
								int pos[] = { 33 };
							} else if (n.getNom() == "F") {
								int pos[] = { 34 };
							} else if (n.getNom() == "G") {
								int pos[] = { 58 };
							}
						} else if (n.getPositions()[0] == 50) { // position 5 B
							if (n.getNom() == "B") {
								int pos[] = { 50 };
							} else if (n.getNom() == "A") {
								int pos[] = { 65 };
							} else if (n.getNom() == "C") {
								int pos[] = { 51 };
							} else if (n.getNom() == "D") {
								int pos[] = { 53 };
							} else if (n.getNom() == "E") {
								int pos[] = { 38 };
							} else if (n.getNom() == "F") {
								int pos[] = { 39 };
							} else if (n.getNom() == "G") {
								int pos[] = { 63 };
							}
						} else if (n.getPositions()[0] == 67) { // position 6 B
							if (n.getNom() == "B") {
								int pos[] = { 67 };
							} else if (n.getNom() == "A") {
								int pos[] = { 65 };
							} else if (n.getNom() == "C") {
								int pos[] = { 68 };
							} else if (n.getNom() == "D") {
								int pos[] = { 53 };
							} else if (n.getNom() == "E") {
								int pos[] = { 55 };
							} else if (n.getNom() == "F") {
								int pos[] = { 56 };
							} else if (n.getNom() == "G") {
								int pos[] = { 58 };
							}
						}
					}
				}
			}
		}
	}

/*	public Tab generateTab(String[] corps, List<Note> note) {
		Tab tab = new Tab();
		Note note2;
		List<Note> list = this.repository.findAll();
		for()
		
		return tab;

	} */
}