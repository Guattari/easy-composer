package ec.easycomposer.v1.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.easycomposer.v1.models.Note;
import ec.easycomposer.v1.services.NoteService;
@CrossOrigin
@RestController
@RequestMapping("notes")
public class NoteController {

	@Autowired
	private NoteService service;

	@GetMapping("")
	public List<Note> findAll() {
		return this.service.findAll();
	}
	/*
	@PostMapping("{id}")
	public Optional<Note> findById(@PathVariable String id) {
		return this.service.findById(id);
	}*/

	@GetMapping("save")
	public void saveNotes() {
		this.service.saveNotes();
	}

	@PostMapping("")
	public Note save(@RequestBody Note entity) {
		return this.service.save(entity);
	}

	@GetMapping("nom/{nom}")
	public Note findByName(@PathVariable String nom) {
		return this.service.findByName(nom);
	}

	@GetMapping("gamme/{gamme}/{nom}")
	public List<Note> choixGammeN(@PathVariable String gamme, @PathVariable String nom) {
		System.out.println(gamme + " " + nom);
		return this.service.choixGamme(gamme, nom);
	}
	@GetMapping("gamme/hz/{gamme}/{hz}")
	public List<Note> choixGammeH(@PathVariable String gamme, @PathVariable String hz) {
		Note note = this.service.findByHz(Float.parseFloat(hz));
		String nom = note.getNom();
		return this.service.choixGamme(gamme, nom);
	}

	@GetMapping("hz/{hz}")
	public Note findByHz(@PathVariable String hz) {
		return this.service.findByHz(Float.parseFloat(hz));
	}

	@GetMapping("minhz/{hz}")
	public Optional<Note> findByMinHz(@PathVariable String hz) {
		return this.service.findByMinHz(Float.parseFloat(hz));
	}

	@GetMapping("maxhz/{hz}")
	public Optional<Note> findByMaxHz(@PathVariable String hz) {
		return this.service.findByMaxHz(Float.parseFloat(hz));
	}
}
