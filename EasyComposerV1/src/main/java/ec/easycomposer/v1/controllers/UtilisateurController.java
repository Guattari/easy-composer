package ec.easycomposer.v1.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.easycomposer.v1.models.Utilisateur;
import ec.easycomposer.v1.repositories.UtilisateurRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("utilisateurs")
public class UtilisateurController {

	@Autowired
	private UtilisateurRepository repository;

	@GetMapping("")
	public List<Utilisateur> findAllByName(@PathVariable String name) {
		return this.repository.findAllByName(name);
	}
	
	@GetMapping("id/{id}")
	private Optional<Utilisateur> findById(@PathVariable String id){
		return this.repository.findById(id);
	}

	@GetMapping("/username/{username}")
	private List<Utilisateur> findAllByUsername(@PathVariable String username) {
		return this.repository.findAllByUsername(username);
	}

	@GetMapping("/name/{name}")
	private List<Utilisateur> findByNameOrFname(@PathVariable String name) {
		return this.repository.findAllByNameOrFname(name, name);
	}
	
	@GetMapping("/email/{email}")
	private List<Utilisateur> findByEmail(@PathVariable String email) {
		return this.repository.findByEmail(email);
	}
	/*
	@PutMapping("/update/{id}")
	private Utilisateur save(@PathVariable("id") @RequestBody Utilisateur entity) {
		return this.repository.save(entity);
	}*/ 
	
	@PostMapping("")
	public Utilisateur save(@RequestBody Utilisateur entity) {
		return this.repository.save(entity);
	}
	
	//@CrossOrigin(origins = "*", allowedHeaders = "*")
	@DeleteMapping("/delete/{id}")
	private void delete(@PathVariable String id){
		 this.repository.deleteById(id);
	} 
	
	
}