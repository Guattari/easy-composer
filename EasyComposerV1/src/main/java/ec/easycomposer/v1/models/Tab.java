package ec.easycomposer.v1.models;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Data;

@Document
@Data
public class Tab {
	@Id
	private String titre;
	private String corps;
	public Tab(String titre, String[] corps) {
		super();
		this.titre = titre;
		this.corps = "e|---------------------------------------------------------------|\r\n" +
				"B|---------------------------------------------------------------|\r\n" +
				"G|---------------------------------------------------------------|\r\n" +
				"D|---------------------------------------------------------------|\r\n" +
				"A|---------------------------------------------------------------|\r\n" +
				"E|---------------------------------------------------------------|";
	}
	public Tab() {
		super();
	}  
}