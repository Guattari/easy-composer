package ec.easycomposer.v1.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class Utilisateur {

	@Id
	private String id;
	private String name;
	private String fname;
	private String password;
	private String username;
	private String experience;
	private int note;
	private String commentaire;
	private String trancheAge;
	@Indexed(unique=true)
	private String email; 

}