package ec.easycomposer.v1.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class Note {
	@Id
	private String id;
	private String nom; // concaténation de type et octave
	private String type; // A LA, B SI, C DO, D Ré, E Mi, F Fa, G Sol
	private int octave;
	private float hz;
	private float hzMin; // définir hzMin = hz*0,97
	private float hzMax; // hzMax = hz*1,035
	private int demiTons; // demi tons d'écart entre la note et le La3 440hz
	private int[] positions;

	public Note(String nom, String type, int octave, float hz, float hzMin, float hzMax, int demiTons, int[] positions) {
		super();
		this.nom = nom;
		this.type = type;
		this.octave = octave;
		this.hz = hz;
		this.hzMin = hzMin;
		this.hzMax = hzMax;
		this.demiTons = demiTons;
		this.positions = positions;
	}

	public Note() {
		super();
	}

}
