package ec.easycomposer.v1.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import ec.easycomposer.v1.models.Note;

public interface NoteRepository extends MongoRepository<Note, String> {
	// public Optional<Note> findByNom(String nom);
	public Note findByNom(String nom);

	public Note findByHz(float hz);

	public Optional<Note> findByHzMin(float hz);

	public Optional<Note> findByHzMax(float hz);
}
