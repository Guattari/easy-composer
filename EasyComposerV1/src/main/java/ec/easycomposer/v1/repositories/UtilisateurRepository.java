package ec.easycomposer.v1.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import ec.easycomposer.v1.models.Utilisateur;

public interface UtilisateurRepository extends MongoRepository<Utilisateur, String> {

	public List<Utilisateur> findAllByName(String name);

	public List<Utilisateur> findAllByUsername(String username);

	public List<Utilisateur> findAllByNameOrFname(String name, String fname);
			
	public List<Utilisateur> findByEmail(String email);
	
	public List<Utilisateur> findAllByTrancheAge(String trancheAge);
	
	public Optional<Utilisateur> findById(String id);

}
