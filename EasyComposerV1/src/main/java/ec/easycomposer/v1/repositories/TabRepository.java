package ec.easycomposer.v1.repositories;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import ec.easycomposer.v1.models.Note;
import ec.easycomposer.v1.models.Tab;


public interface TabRepository extends MongoRepository<Note, String> {
	
	//public List<Tab> findByTitre(String titre);
}