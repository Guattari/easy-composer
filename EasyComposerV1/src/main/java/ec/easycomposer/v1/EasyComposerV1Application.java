package ec.easycomposer.v1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyComposerV1Application {

	public static void main(String[] args) {

		/*
		 * List <Note> listeNote = NoteService.listeNote(); for (Note note : listeNote)
		 * { System.out.println(note); }
		 */

		SpringApplication.run(EasyComposerV1Application.class, args);
	}

}
